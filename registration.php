<?php
/*
 * Copyright © 2021 Wily. All rights reserved.
 * See LICENSE.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Wily_Framework',
    __DIR__
);
