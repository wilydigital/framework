<?php
/*
 * Copyright © 2021 Wily. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wily\Framework\Block\Adminhtml\System\Config\Form\Field\FieldArray;


use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\DataObject;

class TimeArray extends AbstractFieldArray
{
    /**
     * @var Factory
     */
    protected $_elementFactory;

    /**
     * @var string
     */
    protected $_template = 'Wily_Framework::system/config/form/field/time-array.phtml';

    /**
     * TimeSlots constructor.
     * @param Context $context
     * @param Factory $elementFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Factory $elementFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_elementFactory = $elementFactory;
    }

    /**
     *
     */
    protected function _prepareToRender()
    {
        $this->addColumn('from', ['label' => __('From'), 'class' => 'required-entry']);
        $this->addColumn('to', ['label' => __('To'), 'class' => 'required-entry']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * @param string $columnName
     * @return string|string[]
     * @throws \Exception
     */
    public function renderCellTemplate($columnName)
    {
        if (!empty($this->_columns[$columnName]) && in_array($columnName, ['from', 'to'])) {
            $element = $this->_elementFactory->create('time');
            $element->setForm($this->getForm())
                ->setName($this->_getCellInputElementName($columnName))
                ->setHtmlId($this->_getCellInputElementId('<%- _id %>', $columnName));

            return str_replace("\n", '', $element->getElementHtml());
        }

        return parent::renderCellTemplate($columnName);
    }

    /**
     * @param DataObject $row
     */
    protected function _prepareArrayRow(DataObject $row): void
    {
        $options = [];
        $row->setData('option_extra_attrs', $options);
    }

    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = parent::_getElementHtml($element);

        $style = '<style type="text/css">
        #wly_delivery_information_time_configuration_time_slots tbody tr td span.time-separator:nth-of-type(2){display:none}
        #wly_delivery_information_time_configuration_time_slots tbody tr td select:nth-of-type(3){display:none}
        </style>';
        $html .= $style;
        return $html;
    }
}
