<?php
/*
 * Copyright © 2021 Wily. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wily\Framework\Block\Adminhtml\System;


use Magento\Framework\App\State;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\View\Helper\SecureHtmlRenderer;

class Information extends \Magento\Config\Block\System\Config\Form\Fieldset
{
    protected $_directoryList;
    protected $_appState;

    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\View\Helper\Js $jsHelper,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null,
        DirectoryList $directoryList,
        State $appState
    ) {
        parent::__construct($context, $authSession, $jsHelper, $data, $secureRenderer);
        $this->_directoryList = $directoryList;
        $this->_appState = $appState;
    }

    public function render(AbstractElement $element)
    {
        $content = $this->_getHeaderHtml($element);
        $content .= $this->getSystemInformation($element);
        $content .= $this->_getFooterHtml($element);

        return $content;
    }

    public function getSystemInformation($fieldset)
    {
        $content = "";
        $content .= $this->getMagentoRootPath($fieldset);
        $content .= $this->getMagentoDeployMode($fieldset);

        return $content;
    }

    public function getMagentoRootPath($fieldset)
    {
        $label = __("Magento Root Path");
        $path = $this->_directoryList->getRoot();
        return $this->getFieldOutput($fieldset, "magento_root_path", $label, $path);
    }

    public function getMagentoDeployMode($fieldset)
    {
        $label = __("Magento Current Deploy Mode");
        $mode = ucwords($this->_appState->getMode());
        return $this->getFieldOutput($fieldset, "magento_deploy_mode", $label, $mode);
    }

    protected function getFieldOutput($fieldset, $fieldName, $label = '', $value = '')
    {
        $name = strtolower(str_replace(" ", "", $label));
        $field = $fieldset->addField($fieldName, 'label', [
            'name'  => $name,
            'label' => $label,
            'value' => $value,
            'bold' => true
        ]);

        return $field->toHtml();
    }
}
