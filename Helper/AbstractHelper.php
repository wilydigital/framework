<?php
/*
 * Copyright © 2021 Wily. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Wily\Framework\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\Serializer\Serialize;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Serialize\Serializer\Json;

class AbstractHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    const CONFIG_MODULE_PATH = 'wily_core';

    protected $_serialize;
    protected $_json;

    public function __construct(
        Context $context,
        Serialize $serialize,
        Json $json
    )
    {
        parent::__construct($context);
        $this->_serialize = $serialize;
        $this->_json = $json;
    }

    public function getConfigValue($field, $scopeValue = null, $scopeType = ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->getValue($field, $scopeType, $scopeValue);
    }

    public function getGeneralConfiguration($code = '', $storeId = null)
    {
        $code = ($code !== '') ? '/' . $code : '';

        return $this->getConfigValue(static::CONFIG_MODULE_PATH . '/general_configuration' . $code, $storeId);
    }

    public function getSpecificConfiguration($field = '', $storeId = null)
    {
        $field = ($field !== '') ? '/' . $field : '';

        return $this->getConfigValue(static::CONFIG_MODULE_PATH . $field, $storeId);
    }

    public function serialize($data)
    {
        return $this->_serialize->serialize($data);
    }

    public function unserialize($string)
    {
        return $this->_serialize->unserialize($string);
    }

    public function jsonEncode($valueToEncode)
    {
        if($valueToEncode != null) {
            try {
                $encodeValue = $this->_json->serialize($valueToEncode);
            } catch (Exception $e) {
                $encodeValue = '{}';
            }

            return $encodeValue;
        }
    }

    public function jsonDecode($encodedValue)
    {
        if($encodedValue != null) {
            try {
                $decodeValue = $this->_json->unserialize($encodedValue);
            } catch (Exception $e) {
                $decodeValue = [];
            }

            return $decodeValue;
        }
    }
}
